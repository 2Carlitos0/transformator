from Densidad import densidad
from Ecuaciones import sectionNucle, potenciaNucle, numeroVueltas, calculoPotencia, calculoSeccion
from Imprimir import printMessage
from ClearTerminal import clear
'''
  LIMPIEZA DE LA CONSOLA
'''
clear()

# a = input("Insertar : a")
# b = input("Insertar : b")

a = 3 # escrito en [cm]
b = 2.6 # escrito en [cm]
voltagePrimario = 220 #voltaje de entrada [V]
voltagesSecundarios = [110] #voltajes de salida [V]
printMessage("Datos")
print("==============================================================================")
printMessage(" a :> "+str(a) + "[cm]")
printMessage(" b :> "+str(b) + "[cm]")
printMessage("Voltaje de Entrada :> "+str(voltagePrimario)+ "[V]")
for i in voltagesSecundarios:
  printMessage("Voltaje de Salida :> "+str(i)+ "[V]")

sectionNucle = sectionNucle(a,b)
potenciaNucle = potenciaNucle(sectionNucle)
print("==============================================================================")
print("Resultados")
print("==============================================================================")
printMessage("seccion del nucleo:> " + str(sectionNucle))
print("==============================================================================")
printMessage("potencia del nucleo:> " + str(potenciaNucle))

'''
  NUMERO DE VUELTAS DEL PRIMARIO
'''
nPrimario = numeroVueltas(voltagePrimario, sectionNucle)
print("==============================================================================")
printMessage("numero de vueltas del primario:> " + str(nPrimario))
'''
  NUMERO DE VUELTAS DEL SECUNDARIO
'''
print("==============================================================================")

for i in voltagesSecundarios:
  nSecundario = numeroVueltas(i, sectionNucle)
  printMessage("numero de vueltas del secundario:> " + str(nSecundario))

print("==============================================================================")

'''
  DENSIDAD DEL PRIMARIO
'''
densidadMaxima = densidad(potenciaNucle)

printMessage("Densidad Maxima :> " + str(densidadMaxima))

print("==============================================================================")

'''
  CALCULO DE LA CORRIENTE PRIMARIA
'''
intensidadPrimaria =  calculoPotencia(potenciaNucle, voltagePrimario)
printMessage("Intensidad del primario :> " + str(intensidadPrimaria))
'''
  SECCION PRIMARIA
'''
seccionPrimaria = calculoSeccion(intensidadPrimaria, densidadMaxima)
printMessage("Seccion Primaria :> " + str(seccionPrimaria))

print("==============================================================================")

'''
  CALCULO DE LA CORRIENTES SECUNDARIAS
'''
for i in voltagesSecundarios:
  intensidadSecundarios =  calculoPotencia(potenciaNucle, i)
  printMessage("Intensidad de los secundarios :> " + str(intensidadSecundarios))
  '''
    SECCION SECUNDARIA
  '''
  seccionSecundaria = calculoSeccion(intensidadSecundarios, densidadMaxima)
  printMessage("Seccion Secundaria :> " + str(seccionSecundaria))
print("==============================================================================")
