import math

CONSTANTE = 0.0000000444 # 4,44E-8
FRECUENCIA = 50 # Frecuencia de 50[Hz]
BOL = 10000 # 10.000

def sectionNucle(a, b):
  return (a * b)

def potenciaNucle(sectionNucle):
  return (math.pow((sectionNucle / 1.1), 2))

def numeroVueltas(voltage, sectionNucle):
  return voltage / (FRECUENCIA * sectionNucle * BOL * CONSTANTE)

def calculoPotencia(potencia, voltage):
  return potencia / voltage

def calculoSeccion(intensidad, densidadMaxima):
  return intensidad / densidadMaxima