# Densidad maxima en [A] x [mm]^2
# 0-50 = 4
# 50-100 = 3.5
# 100-200 = 3 
# 200=400 = 2.5 

def densidad(potencia):
  densidad = 4
  if(potencia > 50 and potencia < 101):
    densidad = 3.5
  elif(potencia > 99 and potencia < 201):
    densidad = 3
  elif(potencia > 199 and potencia < 401):
    densidad = 2.5
  return densidad
